import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default function Sidebar(props) {
    const [sizeActive, setSizeActive] = useState(false);
    const [path, setPath] = useState("");
    const toggleSize = () => {
        setSizeActive(!sizeActive);
    };

    useEffect(() => {
        // setPath(props.history.url.pathname);
        console.log("path: ", props);
    }, []);

    return (
        <div className="left-sidebar">
            <div>
                <Link className="back-link" to="/">
                    <span>back to home</span>
                </Link>
            </div>
            <ul className="navbar-nav">
                <li className="nav-item nav-profile">
                    <a href="#" className="nav-link">
                        <div className="profile-image">
                            <img
                                className="img-xs rounded-circle"
                                src=""
                                alt="img"
                            />
                            <div className="dot-indicator bg-success"></div>
                        </div>
                        <div className="profile-txt">
                            <p className="profile-name">Tamal Mindaugas</p>
                            <p className="designation">Premium user</p>
                        </div>
                    </a>
                </li>
                <li className="nav-item cat-li">
                    <Link className="nav-link cat-link" to="/admin/categories">
                        <span className="menu-title">Categories</span>
                    </Link>
                </li>

                <li className="nav-item cat-li">
                    <Link className="nav-link cat-link" to="/admin/colors">
                        <span className="menu-title">Color</span>
                    </Link>
                </li>
                <li
                    className={
                        sizeActive
                            ? "nav-item cat-li active"
                            : "nav-item cat-li"
                    }
                >
                    <span className="nav-link cat-link" onClick={toggleSize}>
                        <span className="menu-title">Size</span>
                        <i className="fas fa-arrow-right"></i>
                    </span>
                    <div className="sub-menu">
                        <ul className="nav flex-column">
                            <li className="nav-item cat-li">
                                <Link
                                    className="nav-link cat-link"
                                    to="/admin/men-size"
                                >
                                    <span className="menu-title">Men Size</span>
                                </Link>
                            </li>
                            <li className="nav-item cat-li">
                                <Link
                                    className="nav-link cat-link"
                                    to="/admin/women-size"
                                >
                                    <span className="menu-title">
                                        Women Size
                                    </span>
                                </Link>
                            </li>
                            <li className="nav-item cat-li">
                                <Link
                                    className="nav-link cat-link"
                                    to="/admin/kid-boy-size"
                                >
                                    <span className="menu-title">
                                        Kid Boy Size
                                    </span>
                                </Link>
                            </li>
                            <li className="nav-item cat-li">
                                <Link
                                    className="nav-link cat-link"
                                    to="/admin/kid-girl-size"
                                >
                                    <span className="menu-title">
                                        Kid Girl Size
                                    </span>
                                </Link>
                            </li>
                            <li className="nav-item cat-li">
                                <Link
                                    className="nav-link cat-link"
                                    to="/admin/babies-size"
                                >
                                    <span className="menu-title">
                                        Babies unisex Size
                                    </span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </li>

                <li className="nav-item cat-li">
                    <Link className="nav-link cat-link" to="/admin/brand">
                        <span className="menu-title">Brand</span>
                    </Link>
                </li>
                <li className="nav-item cat-li">
                    <Link className="nav-link cat-link" to="/admin/season">
                        <span className="menu-title">Season</span>
                    </Link>
                </li>
                <li className="nav-item cat-li">
                    <Link className="nav-link cat-link" to="/admin/material">
                        <span className="menu-title">Material</span>
                    </Link>
                </li>
                <li className="nav-item cat-li">
                    <Link className="nav-link cat-link" to="/admin/countries">
                        <span className="menu-title">Countries</span>
                    </Link>
                </li>
                <li className="nav-item cat-li">
                    <Link className="nav-link cat-link" to="/admin/carrier">
                        <span className="menu-title">Carrier</span>
                    </Link>
                </li>
            </ul>
        </div>
    );
}
