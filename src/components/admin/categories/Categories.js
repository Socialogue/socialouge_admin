import React, { Component, useEffect, useState } from "react";
import { axiosInstance } from "../../../utils/axios";
import { add_category, get_categories } from "../../../utils/constants";
import Assign from "../../Assign";
import Table from "../Table";

export default class Categories extends Component {
    state = {
        mode: "add",
        showModal: false,
        name: "",
        sizes: [],
        categoryId: "",
        categories: [],
        color: "",
        menClothes: "",
        menPents: "",
        menShoes: "",
        womenClothes: "",
        womenPents: "",
        womenShoes: "",
        kidBoysClothes: "",
        kidBoysPents: "",
        kidBoysShoes: "",
        kidGirlsClothes: "",
        kidGirlsPents: "",
        kidGirlsShoes: "",
        babiesClothes: "",
        babiesPents: "",
    };

    componentDidMount = async () => {
        const axiosResponse = await axiosInstance.get(get_categories);
        console.log("get categories: ", axiosResponse);
        this.setState({
            categories: axiosResponse.data.categories,
            // sizes: axiosResponse.data.categories.sizes,
        });
        // console.log("size: ", axiosResponse.data.categories.sizes);
    };

    config = [
        {
            label: "Category",
            key: "name",
        },
    ];
    actions = [
        {
            label: "Assign",
            icon: "fa fa-share-alt",
            className: "btn-info",
            onClick: async (id) => {
                console.log(`assign ${id}`);
                const sizes = this.state.categories.filter(
                    (cat) => cat._id == id
                );
                console.log("sizes: ", sizes);
                this.setState({
                    showModal: !this.state.showModal,
                    categoryId: id,
                    sizes: sizes[0],
                });
            },
        },
        {
            label: "View",
            icon: "fa fa-eye",
            className: "btn-primary",
            onClick: async (id) => {
                console.log(`view ${id}`);
                const viewCategory = this.state.categories.find(
                    (category) => category._id == id
                );
                this.props.history.push(
                    `/admin/categories/${id}/sub-categories`
                );
            },
        },
        {
            label: "Edit",
            icon: "fa fa-edit",
            className: "btn-primary",
            onClick: async (id) => {
                this.setState({ mode: "edit" });
                console.log(`editing ${id}`);
                const editCategory = this.state.categories.find(
                    (category) => category._id == id
                );
                console.log(editCategory);
                this.setState({ name: editCategory.name });
                this.setState({ categoryId: editCategory._id });
            },
        },
        {
            label: "Delete",
            icon: "fa fa-trash",
            className: "btn-danger",
            onClick: async (id) => {
                console.log(`deleting ${id}`);
                const deleteCategory = this.state.categories.find(
                    (category) => category._id == id
                );
                console.log(deleteCategory);
                const con = window.confirm("are you sure?");
                if (con) {
                    const deleteResponse = await axiosInstance.delete(
                        get_categories + deleteCategory._id
                    );
                    console.log(deleteResponse);
                    if (
                        deleteResponse.status == 200 &&
                        deleteResponse.data.success == true
                    ) {
                        alert("Category deleted successfully.");
                    } else {
                        alert("Something went wrong! please try again.");
                    }
                }
            },
        },
    ];

    onChange = (e) => {
        this.setState({ name: e.target.value });
    };

    submit = async (e) => {
        console.log("submit: ", this.state.name);
        if (this.state.name == "") {
            alert("Please enter a category name.");
            return;
        }
        if (this.state.mode == "add") {
            const response = await axiosInstance.post(add_category, {
                name: this.state.name,
            });
            console.log("res: ", response);
            if (response.status == 201 && response.data.success == true) {
                alert("Your category added successfully.");
                this.setState({
                    name: "",
                    mode: "add",
                    categoryId: "",
                });
            } else {
                alert("Something went wrong! please try again.");
            }
        } else {
            const response = await axiosInstance.patch(
                get_categories + this.state.categoryId,
                {
                    name: this.state.name,
                }
            );
            if (response.status == 200 && response.data.success == true) {
                alert("Your category updated successfully.");
                this.setState({
                    name: "",
                    mode: "add",
                    categoryId: "",
                });
            } else {
                alert("Something went wrong! please try again.");
            }
        }
    };

    toggleModal = () => {
        this.setState({ showModal: !this.state.showModal });
    };

    onChangeAssign = (e, id) => {
        const name = e.target.name;
        const value = e.target.checked;
        console.log("assign valu:", name, value, id);
        if (id == "color") {
            this.setState({ [name]: value });

            return;
        }
        this.setState({
            [name]: id,
        });
    };

    submitAssign = async () => {
        const assignResponse = await axiosInstance.patch(
            get_categories + this.state.categoryId,
            {
                color: this.state.color,
                sizes: [
                    this.state.menClothes,
                    this.state.menPents,
                    this.state.menShoes,
                    this.state.womenClothes,
                    this.state.womenPents,
                    this.state.womenShoes,
                    this.state.kidBoysClothes,
                    this.state.kidBoysPents,
                    this.state.kidBoysShoes,
                    this.state.kidGirlsClothes,
                    this.state.kidGirlsPents,
                    this.state.kidGirlsShoes,
                    this.state.babiesClothes,
                    this.state.babiesPents,
                ],
            }
        );
        this.toggleModal();
        console.log("res: ", assignResponse);

        if (assignResponse.status == 200 || 201) {
            alert("Sizes and color assign successfully");
        } else {
            alert("Something went wrong! please try again.");
        }
    };

    render() {
        return (
            <>
                <div className="category-input">
                    <div className="row w-100">
                        <div className="col-12 col-md-6 col-lg-4">
                            <div className="form-group">
                                <label>
                                    <h6 className="">Category</h6>
                                </label>
                                <div className="form-input">
                                    <input
                                        type="text"
                                        value={this.state.name}
                                        className="form-control"
                                        placeholder="Enter a category name"
                                        onChange={(e) => this.onChange(e)}
                                    />
                                </div>
                            </div>
                            {/* {categoryError !== '' && (
            <div style={{ color: '#E46470' }}>{categoryError}</div>
          )}
          {categorySuccess !== '' && (
            <div style={{ color: '#4BB543' }}>{categorySuccess}</div>
          )} */}
                        </div>
                        <div className="col-4 col-lg-4">
                            <label className="d-none d-md-flex">
                                <h6 className="invisible">Categories</h6>
                            </label>
                            <div className="form-group">
                                <button
                                    className="btn add-btn"
                                    onClick={this.submit}
                                >
                                    <i className="fas fa-plus-circle"></i>
                                    {this.state.mode && this.state.mode == "add"
                                        ? "Add"
                                        : "Update"}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <Table
                    data={this.state.categories}
                    config={this.config}
                    actions={this.actions}
                />

                {/* assign modal... */}

                <Assign
                    showModal={this.state.showModal}
                    toggleModal={this.toggleModal}
                    onChangeAssign={this.onChangeAssign}
                    submitAssign={this.submitAssign}
                    data={this.state.sizes}
                />
            </>
        );
    }
}
