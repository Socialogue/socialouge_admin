import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { routes } from "./routes/index";
import Header from "./components/admin/header/Header";
import Sidebar from "./components/admin/leftbar/Sidebar";

function App() {
    return (
        <>
            <Router>
                <Header />
                <div className="page-body-wrapper d-flex">
                    <Sidebar />
                    <div className="main-body p-4">
                        <Switch>
                            {routes.map((route) => (
                                <Route
                                    key={route.path}
                                    path={route.path}
                                    component={route.component}
                                    exact={route.exact}
                                />
                            ))}
                        </Switch>
                    </div>
                </div>
            </Router>
        </>
    );
}

export default App;
