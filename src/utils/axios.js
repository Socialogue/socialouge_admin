import axios from "axios";
import { api_base_url } from "../utils/constants";

export const axiosInstance = axios.create({
    baseURL: api_base_url,
});
