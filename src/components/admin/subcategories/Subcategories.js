import React, { Component, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { axiosInstance } from "../../../utils/axios";
import {
    add_sub_category,
    get_categories,
    get_sub_categories,
    delete_sub_categories,
} from "../../../utils/constants";
import Assign from "../../Assign";
import Table from "../Table";

export default class Subcategories extends Component {
    state = {
        mode: "add",
        showModal: false,
        name: "",
        category: "",
        categoryId: "",
        subcategoryId: "",
        categories: [],
        subCategories: [],
        color: "",
        menClothes: "",
        menPents: "",
        menShoes: "",
        womenClothes: "",
        womenPents: "",
        womenShoes: "",
        kidBoysClothes: "",
        kidBoysPents: "",
        kidBoysShoes: "",
        kidGirlsClothes: "",
        kidGirlsPents: "",
        kidGirlsShoes: "",
        babiesClothes: "",
        babiesPents: "",
    };

    componentDidMount = async () => {
        this.setState({ categoryId: this.props.match.params.catId });
        const axiosResponse = await axiosInstance.get(
            get_sub_categories + this.props.match.params.catId
        );
        console.log("get sub categories: ", axiosResponse);
        this.setState({
            subCategories: axiosResponse.data.data[0].subCategory,
        });
        this.setState({ category: axiosResponse.data.data[0].name });
    };

    config = [
        {
            label: "Sub-Category",
            key: "name",
        },
    ];
    actions = [
        {
            label: "Assign",
            icon: "fa fa-share-alt",
            className: "btn-info",
            onClick: async (id) => {
                console.log(`assign ${id}`);
                this.setState({
                    showModal: !this.state.showModal,
                    subcategoryId: id,
                });
            },
        },
        {
            label: "View",
            icon: "fa fa-eye",
            className: "btn-primary",
            onClick: async (id) => {
                console.log(`view ${id}`);
                const viewSubCategory = this.state.subCategories.find(
                    (subcategory) => subcategory._id == id
                );
                this.props.history.push(
                    `/admin/categories/${this.state.categoryId}/sub-categories/${id}/inner-categories`
                );
            },
        },
        {
            label: "Edit",
            icon: "fa fa-edit",
            className: "btn-primary",
            onClick: async (id) => {
                this.setState({ mode: "edit" });
                console.log(`editing ${id}`);
                const editCategory = this.state.subCategories.find(
                    (category) => category._id == id
                );
                console.log(editCategory);
                this.setState({ name: editCategory.name });
                this.setState({ categoryId: editCategory._id });
            },
        },
        {
            label: "Delete",
            icon: "fa fa-trash",
            className: "btn-danger",
            onClick: async (id) => {
                console.log(`deleting ${id}`);

                const con = window.confirm("are you sure?");
                if (con) {
                    const categoryId = this.state.categoryId;
                    const deleteResponse = await axiosInstance.delete(
                        delete_sub_categories + id,
                        {
                            data: {
                                categoryId,
                            },
                        }
                    );
                    console.log(deleteResponse);
                    if (
                        deleteResponse.status == 200 &&
                        deleteResponse.data.success == true
                    ) {
                        alert("Category deleted successfully.");
                    } else {
                        alert("Something went wrong! please try again.");
                    }
                }
            },
        },
    ];

    onChange = (e) => {
        this.setState({ name: e.target.value });
    };

    submit = async (e) => {
        console.log("submit: ", this.state.name);
        if (this.state.name == "") {
            alert("Please enter a sub-category name.");
            return;
        }
        if (this.state.mode == "add") {
            const response = await axiosInstance.post(add_sub_category, {
                name: this.state.name,
                categoryId: this.state.categoryId,
            });
            console.log("res: ", response);
            if (response.status == 201 && response.data.success == true) {
                alert("Your sub-category added successfully.");
                this.setState({
                    name: "",
                    mode: "add",
                    categoryId: "",
                });
            } else {
                alert("Something went wrong! please try again.");
            }
        } else {
            const response = await axiosInstance.patch(
                delete_sub_categories + this.state.categoryId,
                {
                    name: this.state.name,
                }
            );
            if (response.status == 200 && response.data.success == true) {
                alert("Your category updated successfully.");
                this.setState({
                    name: "",
                    mode: "add",
                    categoryId: "",
                });
            } else {
                alert("Something went wrong! please try again.");
            }
        }
    };

    toggleModal = () => {
        this.setState({ showModal: !this.state.showModal });
    };

    onChangeAssign = (e, id) => {
        const name = e.target.name;
        const value = e.target.checked;
        console.log("assign valu:", name, value, id);
        if (id == "color") {
            this.setState({ [name]: value });

            return;
        }
        this.setState({
            [name]: value ? id : "",
        });
    };

    submitAssign = async () => {
        const dt = {
            color: this.state.color,
            sizes: [
                this.state.menClothes,
                this.state.menPents,
                this.state.menShoes,
                this.state.womenClothes,
                this.state.womenPents,
                this.state.womenShoes,
                this.state.kidBoysClothes,
                this.state.kidBoysPents,
                this.state.kidBoysShoes,
                this.state.kidGirlsClothes,
                this.state.kidGirlsPents,
                this.state.kidGirlsShoes,
                this.state.babiesClothes,
                this.state.babiesPents,
            ],
        };
        console.log("data; ", dt);
        const assignResponse = await axiosInstance.patch(
            delete_sub_categories + this.state.subcategoryId,
            {
                color: this.state.color,
                sizes: [
                    this.state.menClothes,
                    this.state.menPents,
                    this.state.menShoes,
                    this.state.womenClothes,
                    this.state.womenPents,
                    this.state.womenShoes,
                    this.state.kidBoysClothes,
                    this.state.kidBoysPents,
                    this.state.kidBoysShoes,
                    this.state.kidGirlsClothes,
                    this.state.kidGirlsPents,
                    this.state.kidGirlsShoes,
                    this.state.babiesClothes,
                    this.state.babiesPents,
                ],
            }
        );
        this.toggleModal();
        console.log("res: ", assignResponse);

        if (assignResponse.status == 200 || 201) {
            alert("Sizes and color assign successfully");
        } else {
            alert("Something went wrong! please try again.");
        }
    };

    render() {
        return (
            <>
                <div className="category-input">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <Link to="/admin/categories">
                                    {this.state.category}
                                </Link>
                            </li>
                        </ol>
                    </nav>
                    <div className="row w-100">
                        <div className="col-12 col-md-6 col-lg-4">
                            <div className="form-group">
                                <label>
                                    <h6 className="">Sub-category</h6>
                                </label>
                                <div className="form-input">
                                    <input
                                        type="text"
                                        value={this.state.name}
                                        className="form-control"
                                        placeholder="Enter a sub-category name"
                                        onChange={(e) => this.onChange(e)}
                                    />
                                </div>
                            </div>
                            {/* {categoryError !== '' && (
            <div style={{ color: '#E46470' }}>{categoryError}</div>
          )}
          {categorySuccess !== '' && (
            <div style={{ color: '#4BB543' }}>{categorySuccess}</div>
          )} */}
                        </div>
                        <div className="col-4 col-lg-4">
                            <label className="d-none d-md-flex">
                                <h6 className="invisible">Categories</h6>
                            </label>
                            <div className="form-group">
                                <button
                                    className="btn add-btn"
                                    onClick={this.submit}
                                >
                                    <i className="fas fa-plus-circle"></i>
                                    {this.state.mode && this.state.mode == "add"
                                        ? "Add"
                                        : "Update"}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <Table
                    data={this.state.subCategories}
                    config={this.config}
                    actions={this.actions}
                />

                {/* assign modal... */}

                <Assign
                    showModal={this.state.showModal}
                    toggleModal={this.toggleModal}
                    onChangeAssign={this.onChangeAssign}
                    submitAssign={this.submitAssign}
                />
            </>
        );
    }
}
