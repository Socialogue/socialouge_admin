import React from "react";
import Table from "../Table";

export default function Subcategories() {
  const data = [
    {
      id: 1,
      nr: 1,
      kategorija: "category",
      sub_kategorija: "sub category",
      inner_category: "inner category",
    },
    {
      id: 2,
      nr: 2,
      kategorija: "category 1",
      sub_kategorija: "sub category 1",
    },
  ];
  const config = [
    {
      label: "Nr.",
      key: "nr",
    },
    {
      label: "Kategorija",
      key: "kategorija",
    },
    {
      label: "Sub-Kategorija",
      key: "sub_kategorija",
    },
  ];

  const actions = [
    {
      label: "Edit",
      icon: "fa fa-edit",
      className: "btn-primary",
      onClick: (id) => {
        console.log(`editing ${id}`);
      },
    },
    {
      label: "Delete",
      icon: "fa fa-trash",
      className: "btn-danger",
      onClick: (id) => {
        console.log(`deleting ${id}`);
      },
    },
  ];

  const handler = [
    {
      label: "Category",
      className: "",
      placeholder: "Enter a category name",
      onChange: (id) => {
        console.log(`${id.target.value}`);
      },
    },
    {
      label: "Sub Category",
      className: "",
      placeholder: "Enter a sub category name",
      onChange: (id) => {
        console.log(`${id.target.value}`);
      },
    },
  ];

  return (
    <>
      <Table
        header="Sub Category"
        data={data}
        config={config}
        actions={actions}
        handler={handler}
      />
    </>
  );
}
