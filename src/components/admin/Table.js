import React, { useState } from "react";
import PropTypes from "prop-types";

import {
    Modal,
    ModalHeader,
    ModalBody,
    Input,
    ModalFooter,
    Button,
} from "reactstrap";

export default function Table(props) {
    const [category, setCategory] = useState("");

    const handleCategory = (e) => {
        setCategory(e.target.value);
        // console.log(e.target.value);
    };

    const handleCategoryChange = (e) => {
        console.log(e.target.value);
    };
    return (
        <div className="category-list-cnt">
            {/* update and delete  */}

            <Modal centered={true}>
                <ModalHeader>Name: laskdf</ModalHeader>
                <ModalBody>
                    <div className="row w-100 mr-0">
                        <div className="col-12 col-md-6 pr-0">
                            <div className="form-group">
                                <label>
                                    <h6 className="">Category</h6>
                                </label>
                                <div className="form-input">
                                    <Input
                                        className="form-control"
                                        type="text"
                                        name="category"
                                        onChange={handleCategory}
                                        value={category}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary">Confirm Update</Button>
                </ModalFooter>
            </Modal>

            <Modal centered={true}>
                <ModalHeader>category</ModalHeader>
                <ModalBody>
                    <div
                        style={{
                            background: "#FBE9E7",
                            padding: "10px",
                            color: "#D32F2F",
                        }}
                    >
                        This will permanently delete
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button color="danger">Confirm Delete</Button>
                </ModalFooter>
            </Modal>

            {/* assign modal  */}

            {/* <AssignColorAndSize
       
        categoryType="categories"
      /> */}

            <div className="category-list">
                <table className="table table-striped mb-0 category-table">
                    <thead>
                        <tr>
                            {props.config &&
                                props.config.map((th) => (
                                    <th
                                        className="no-th"
                                        scope="col"
                                        key={th.key}
                                    >
                                        {th.label}
                                    </th>
                                ))}
                            <th className="no-th" scope="col" key="val">
                                Valdymas
                            </th>
                            {/* <th className="no-th" scope="col">
                Nr.
              </th>
              <th scope="col">Kategorija</th>
              <th scope="col">Valdymas</th> */}
                        </tr>
                    </thead>

                    <tbody>
                        {props.data &&
                            props.data.map((row) => (
                                <tr>
                                    {props.config &&
                                        props.config.map((item) => (
                                            <td
                                                className="no-th"
                                                scope="row"
                                                key={item.key}
                                            >
                                                {row[item.key]}
                                            </td>
                                        ))}

                                    <td>
                                        {props.actions &&
                                            props.actions.map((action) => (
                                                <button
                                                    className={action.className}
                                                    onClick={() =>
                                                        action.onClick(row._id)
                                                    }
                                                >
                                                    <i
                                                        className={action.icon}
                                                    ></i>
                                                </button>
                                            ))}
                                    </td>
                                </tr>
                            ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
