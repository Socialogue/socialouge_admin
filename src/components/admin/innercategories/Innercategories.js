import React, { Component, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { axiosInstance } from "../../../utils/axios";
import {
    add_inner_category,
    delete_inner_categories,
    get_categories,
    get_inner_categories,
} from "../../../utils/constants";
import Assign from "../../Assign";
import Table from "../Table";

export default class Innercategories extends Component {
    state = {
        mode: "add",
        showModal: false,
        name: "",
        category: "",
        categoryId: "",
        subCategory: "",
        subCategoryId: "",
        innerCategory: "",
        innerCategoryId: "",
        categories: [],
        subCategories: [],
        innerCategories: [],
        color: "",
        menClothes: "",
        menPents: "",
        menShoes: "",
        womenClothes: "",
        womenPents: "",
        womenShoes: "",
        kidBoysClothes: "",
        kidBoysPents: "",
        kidBoysShoes: "",
        kidGirlsClothes: "",
        kidGirlsPents: "",
        kidGirlsShoes: "",
        babiesClothes: "",
        babiesPents: "",
    };

    componentDidMount = async () => {
        this.setState({ categoryId: this.props.match.params.catId });
        this.setState({ subCategoryId: this.props.match.params.subCatId });
        const axiosResponse = await axiosInstance.get(
            get_inner_categories + this.props.match.params.subCatId
        );

        this.setState({
            innerCategories:
                axiosResponse.data.data[0].subCategory.innerCategory,
        });
        this.setState({ category: axiosResponse.data.data[0].name });
        this.setState({
            subCategory: axiosResponse.data.data[0].subCategory.name,
        });
        console.log("cate: ", this.state.innerCategories);
    };

    config = [
        {
            label: "Inner-Category",
            key: "name",
        },
    ];

    actions = [
        {
            label: "Assign",
            icon: "fa fa-share-alt",
            className: "btn-info",
            onClick: async (id) => {
                console.log(`assign ${id}`);
                this.setState({
                    showModal: !this.state.showModal,
                    innerCategoryId: id,
                });
            },
        },
        {
            label: "View",
            icon: "fa fa-eye",
            className: "btn-primary",
            onClick: async (id) => {
                console.log(`view ${id}`);
                const viewSubCategory = this.state.innerCategories.find(
                    (subcategory) => subcategory._id == id
                );
                this.props.history.push(
                    `/admin/categories/${this.state.categoryId}/sub-categories/${this.state.subCategoryId}/inner-categories/${id}/inner-categories-two`
                );
            },
        },
        {
            label: "Edit",
            icon: "fa fa-edit",
            className: "btn-primary",
            onClick: async (id) => {
                this.setState({ mode: "edit" });
                console.log(`editing ${id}`);
                const editCategory = this.state.innerCategories.find(
                    (category) => category._id == id
                );
                console.log(editCategory);
                this.setState({ name: editCategory.name });
                this.setState({ categoryId: editCategory._id });
            },
        },
        {
            label: "Delete",
            icon: "fa fa-trash",
            className: "btn-danger",
            onClick: async (id) => {
                console.log(`deleting ${id}`);

                const con = window.confirm("are you sure?");
                if (con) {
                    const subCategoryId = this.state.subCategoryId;
                    const deleteResponse = await axiosInstance.delete(
                        delete_inner_categories + id,
                        {
                            data: {
                                subCategoryId,
                            },
                        }
                    );
                    console.log(deleteResponse);
                    if (
                        deleteResponse.status == 200 &&
                        deleteResponse.data.success == true
                    ) {
                        alert("Category deleted successfully.");
                    } else {
                        alert("Something went wrong! please try again.");
                    }
                }
            },
        },
    ];

    onChange = (e) => {
        this.setState({ name: e.target.value });
    };

    submit = async (e) => {
        if (this.state.name == "") {
            alert("Please enter a inner category name.");
            return;
        }
        if (this.state.mode == "add") {
            const response = await axiosInstance.post(add_inner_category, {
                name: this.state.name,
                subCategoryId: this.state.subCategoryId,
            });
            if (response.status == 201 && response.data.success == true) {
                alert("Your inner category added successfully.");
                this.setState({ name: "", mode: "add", categoryId: "" });
            } else {
                alert("Something went wrong! please try again.");
            }
        } else {
            const response = await axiosInstance.patch(
                delete_inner_categories + this.state.categoryId,
                {
                    name: this.state.name,
                }
            );
            if (response.status == 200 && response.data.success == true) {
                alert("Your category updated successfully.");
                this.setState({ name: "", mode: "add", categoryId: "" });
            } else {
                alert("Something went wrong! please try again.");
            }
        }
    };

    toggleModal = () => {
        this.setState({ showModal: !this.state.showModal });
    };

    onChangeAssign = (e, id) => {
        const name = e.target.name;
        const value = e.target.checked;
        console.log("assign valu:", name, value, id);
        if (id == "color") {
            this.setState({ [name]: value });

            return;
        }
        this.setState({
            [name]: value ? id : "",
        });
    };

    submitAssign = async () => {
        const dt = {
            color: this.state.color,
            sizes: [
                this.state.menClothes,
                this.state.menPents,
                this.state.menShoes,
                this.state.womenClothes,
                this.state.womenPents,
                this.state.womenShoes,
                this.state.kidBoysClothes,
                this.state.kidBoysPents,
                this.state.kidBoysShoes,
                this.state.kidGirlsClothes,
                this.state.kidGirlsPents,
                this.state.kidGirlsShoes,
                this.state.babiesClothes,
                this.state.babiesPents,
            ],
        };
        console.log("data; ", dt);
        const assignResponse = await axiosInstance.patch(
            delete_inner_categories + this.state.innerCategoryId,
            {
                color: this.state.color,
                sizes: [
                    this.state.menClothes,
                    this.state.menPents,
                    this.state.menShoes,
                    this.state.womenClothes,
                    this.state.womenPents,
                    this.state.womenShoes,
                    this.state.kidBoysClothes,
                    this.state.kidBoysPents,
                    this.state.kidBoysShoes,
                    this.state.kidGirlsClothes,
                    this.state.kidGirlsPents,
                    this.state.kidGirlsShoes,
                    this.state.babiesClothes,
                    this.state.babiesPents,
                ],
            }
        );
        this.toggleModal();
        console.log("res: ", assignResponse);

        if (assignResponse.status == 200 || 201) {
            alert("Sizes and color assign successfully");
        } else {
            alert("Something went wrong! please try again.");
        }
    };

    render() {
        return (
            <>
                <div className="category-input">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <Link to="/admin/categories">
                                    {this.state.category}
                                </Link>
                            </li>
                            <li class="breadcrumb-item">
                                <Link
                                    to={`/admin/categories/${this.state.categoryId}/sub-categories`}
                                >
                                    {this.state.subCategory}
                                </Link>
                            </li>
                        </ol>
                    </nav>
                    <div className="row w-100">
                        <div className="col-12 col-md-6 col-lg-4">
                            <div className="form-group">
                                <label>
                                    <h6 className="">Inner Category</h6>
                                </label>
                                <div className="form-input">
                                    <input
                                        type="text"
                                        value={this.state.name}
                                        className="form-control"
                                        placeholder="Enter a inner-category name"
                                        onChange={(e) => this.onChange(e)}
                                    />
                                </div>
                            </div>
                            {/* {categoryError !== '' && (
      <div style={{ color: '#E46470' }}>{categoryError}</div>
    )}
    {categorySuccess !== '' && (
      <div style={{ color: '#4BB543' }}>{categorySuccess}</div>
    )} */}
                        </div>
                        <div className="col-4 col-lg-4">
                            <label className="d-none d-md-flex">
                                <h6 className="invisible">Categories</h6>
                            </label>
                            <div className="form-group">
                                <button
                                    className="btn add-btn"
                                    onClick={this.submit}
                                >
                                    <i className="fas fa-plus-circle"></i>
                                    {this.state.mode && this.state.mode == "add"
                                        ? "Add"
                                        : "Update"}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <Table
                    data={this.state.innerCategories}
                    config={this.config}
                    actions={this.actions}
                />

                {/* assign modal... */}

                <Assign
                    showModal={this.state.showModal}
                    toggleModal={this.toggleModal}
                    onChangeAssign={this.onChangeAssign}
                    submitAssign={this.submitAssign}
                />
            </>
        );
    }
}
