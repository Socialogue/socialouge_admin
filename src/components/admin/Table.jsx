import React, { useState } from "react";
import PropTypes from "prop-types";

import {
  Modal,
  ModalHeader,
  ModalBody,
  Input,
  ModalFooter,
  Button,
} from "reactstrap";

export default function Table(props) {
  const [category, setCategory] = useState("");

  const handleCategory = (e) => {
    setCategory(e.target.value);
    // console.log(e.target.value);
  };

  const handleCategoryChange = (e) => {
    console.log(e.target.value);
  };
  return (
    <div className="category-list-cnt">
      {/* update and delete  */}

      <Modal centered={true}>
        <ModalHeader>Name: laskdf</ModalHeader>
        <ModalBody>
          <div className="row w-100 mr-0">
            <div className="col-12 col-md-6 pr-0">
              <div className="form-group">
                <label>
                  <h6 className="">Category</h6>
                </label>
                <div className="form-input">
                  <Input
                    className="form-control"
                    type="text"
                    name="category"
                    onChange={handleCategory}
                    value={category}
                  />
                </div>
              </div>
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="primary">Confirm Update</Button>
        </ModalFooter>
      </Modal>

      <Modal centered={true}>
        <ModalHeader>category</ModalHeader>
        <ModalBody>
          <div
            style={{ background: "#FBE9E7", padding: "10px", color: "#D32F2F" }}
          >
            This will permanently delete
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="danger">Confirm Delete</Button>
        </ModalFooter>
      </Modal>

      {/* assign modal  */}

      {/* <AssignColorAndSize
       
        categoryType="categories"
      /> */}

      <div className="category-nav d-none">
        <div className="category-ul mx-3 mx-md-0">
          <ul
            className="nav nav-pills mb-4 m-border-0"
            id="pills-tab"
            role="tablist"
          >
            <li className="nav-item">
              <a className="nav-link active" id="pills-category-tab">
                Kategorijos
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" id="pills-sub-category-tab">
                Sub-Kategorijos
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" id="pills-inner-category-tab">
                Inner-Kategorijos
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" id="pills-inner-category-tab">
                Inner-Kategorijos2
              </a>
            </li>
            <li className="nav-item li-btn d-none d-md-inline-block">
              <a href={"/category-add"}>
                <button className="btn add-btn">
                  <img src="https://img.icons8.com/material-outlined/24/000000/plus--v1.png" />
                  Pridėti kategoriją
                </button>
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div className="category-input">
        <div className="row w-100">
          {props.handler.map((handle) => (
            <div className="col-12 col-md-6 col-lg-4">
              <div className="form-group">
                <label>
                  <h6 className="">{handle.label}</h6>
                </label>
                <div className="form-input">
                  <input
                    type="text"
                    value={category}
                    className="form-control"
                    placeholder={handle.placeholder}
                    onChange={(e) => handle.onChange(e)}
                  />
                </div>
              </div>
              {/* {categoryError !== '' && (
              <div style={{ color: '#E46470' }}>{categoryError}</div>
            )}
            {categorySuccess !== '' && (
              <div style={{ color: '#4BB543' }}>{categorySuccess}</div>
            )} */}
            </div>
          ))}
          <div className="col-4 col-lg-4">
            <label className="d-none d-md-flex">
              <h6 className="invisible">Category</h6>
            </label>
            <div className="form-group">
              <button className="btn add-btn">
                <i className="fas fa-plus-circle"></i>
                Add
              </button>
            </div>
          </div>
        </div>
      </div>

      <div className="category-list">
        <table className="table table-striped mb-0 category-table">
          <thead>
            <tr>
              {props.config.map((th) => (
                <th className="no-th" scope="col" key={th.key}>
                  {th.label}
                </th>
              ))}
              <th className="no-th" scope="col" key="val">
                Valdymas
              </th>
              {/* <th className="no-th" scope="col">
                Nr.
              </th>
              <th scope="col">Kategorija</th>
              <th scope="col">Valdymas</th> */}
            </tr>
          </thead>

          <tbody>
            {props.data.map((row) => (
              <tr>
                {props.config.map((item) => (
                  <td className="no-th" scope="row" key={item.key}>
                    {row[item.key]}
                  </td>
                ))}

                <td>
                  {props.actions.map((action) => (
                    <button
                      className={action.className}
                      onClick={() => action.onClick(row.id)}
                    >
                      <i className={action.icon}></i>
                    </button>
                  ))}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
