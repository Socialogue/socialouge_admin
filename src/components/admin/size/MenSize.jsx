import React from "react";
import Table from "../Table";

export default function MenSize() {
  const data = [
    {
      id: 1,
      eu: 1,
      uk: 2,
      us: 3,
      it: 4,
    },
    {
      id: 2,
      eu: 11,
      uk: 21,
      us: 31,
      it: 41,
    },
  ];
  const config = [
    {
      label: "EU",
      key: "eu",
    },
    {
      label: "UK",
      key: "uk",
    },
    {
      label: "IT",
      key: "it",
    },
    {
      label: "US",
      key: "us",
    },
  ];

  const actions = [
    {
      label: "Edit",
      icon: "fa fa-edit",
      className: "btn-primary",
      onClick: (id) => {
        console.log(`editing ${id}`);
      },
    },
    {
      label: "Delete",
      icon: "fa fa-trash",
      className: "btn-danger",
      onClick: (id) => {
        console.log(`deleting ${id}`);
      },
    },
  ];

  const handler = [
    {
      label: "Size",
      className: "",
      placeholder: "Enter a size",
      onChange: (id) => {
        console.log(`${id.target.value}`);
      },
    },
  ];

  return (
    <>
      <h2>Men clothes size</h2>
      <Table data={data} config={config} actions={actions} handler={handler} />
      <h2>Men pents size</h2>
      <Table data={data} config={config} actions={actions} handler={handler} />
      <h2>Men shoes size</h2>
      <Table data={data} config={config} actions={actions} handler={handler} />
    </>
  );
}
