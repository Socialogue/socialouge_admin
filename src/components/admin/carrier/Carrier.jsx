import React from "react";
import Table from "../Table";

export default function Carrier() {
  const data = [
    {
      id: 1,
      nr: 1,
      carrier: "delux",
    },
    {
      id: 2,
      nr: 2,
      carrier: "baldai",
    },
  ];
  const config = [
    {
      label: "Nr.",
      key: "nr",
    },
    {
      label: "Carrier",
      key: "carrier",
    },
  ];

  const actions = [
    {
      label: "Edit",
      icon: "fa fa-edit",
      className: "btn-primary",
      onClick: (id) => {
        console.log(`editing ${id}`);
      },
    },
    {
      label: "Delete",
      icon: "fa fa-trash",
      className: "btn-danger",
      onClick: (id) => {
        console.log(`deleting ${id}`);
      },
    },
  ];

  const handler = [
    {
      label: "Country",
      className: "",
      placeholder: "Enter a country name",
      onChange: (id) => {
        console.log(`${id.target.value}`);
      },
    },
    {
      label: "Carrier",
      className: "",
      placeholder: "Enter a carrier name",
      onChange: (id) => {
        console.log(`${id.target.value}`);
      },
    },
  ];
  return (
    <>
      <Table data={data} config={config} actions={actions} handler={handler} />
    </>
  );
}
