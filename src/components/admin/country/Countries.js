import React, { useState, useEffect } from "react";
import { axiosInstance } from "../../../utils/axios";
import { get_countries, add_country } from "../../../utils/constants";
import Table from "../Table";

export default function Countries() {
    const [mode, setMode] = useState("add");
    const [country, setCountry] = useState("");
    const [countryId, setCountryId] = useState("");
    const [countries, setCountries] = useState([]);

    useEffect(async () => {
        const axiosResponse = await axiosInstance.get(get_countries);
        console.log("get countries: ", axiosResponse);
        setCountries(axiosResponse.data.data.docs);
    }, []);

    const config = [
        {
            label: "Nr.",
            key: "_id",
        },
        {
            label: "Country",
            key: "country",
        },
    ];
    const actions = [
        {
            label: "Edit",
            icon: "fa fa-edit",
            className: "btn-primary",
            onClick: async (id) => {
                setMode("edit");
                console.log(`editing ${id}`);
                const editCountry = countries.find(
                    (country) => country._id == id
                );
                console.log(editCountry);
                setCountry(editCountry.country);
                setCountryId(editCountry._id);
            },
        },
        {
            label: "Delete",
            icon: "fa fa-trash",
            className: "btn-danger",
            onClick: async (id) => {
                console.log(`deleting ${id}`);
                const deleteCountry = countries.find(
                    (country) => country._id == id
                );
                console.log(deleteCountry);
                const con = window.confirm("are you sure?");
                if (con) {
                    const deleteResponse = await axiosInstance.delete(
                        get_countries + deleteCountry._id
                    );
                    console.log(deleteResponse);
                    if (
                        deleteResponse.status == 200 &&
                        deleteResponse.data.success == true
                    ) {
                        alert("Country deleted successfully.");
                    } else {
                        alert("Something went wrong! please try again.");
                    }
                }
            },
        },
    ];

    const onChange = (e) => {
        setCountry(e.target.value);
    };

    const submit = async (e) => {
        console.log("submit: ", country);
        if (country == "") {
            alert("Please enter a country name.");
            return;
        }
        if (mode == "add") {
            const response = await axiosInstance.post(add_country, {
                country: country,
            });
            if (response.status == 201 && response.data.success == true) {
                alert("Your country added successfully.");
                setCountry("");
                setMode("add");
                setCountryId("");
            } else {
                alert("Something went wrong! please try again.");
            }
        } else {
            const response = await axiosInstance.patch(
                get_countries + countryId,
                {
                    country: country,
                }
            );
            if (response.status == 200 && response.data.success == true) {
                alert("Your country updated successfully.");
                setCountry("");
                setMode("add");
                setCountryId("");
            } else {
                alert("Something went wrong! please try again.");
            }
        }
    };
    return (
        <>
            <div className="category-input">
                <div className="row w-100">
                    <div className="col-12 col-md-6 col-lg-4">
                        <div className="form-group">
                            <label>
                                <h6 className="">Country</h6>
                            </label>
                            <div className="form-input">
                                <input
                                    type="text"
                                    value={country}
                                    className="form-control"
                                    placeholder="Enter a country name"
                                    onChange={(e) => onChange(e)}
                                />
                            </div>
                        </div>
                        {/* {categoryError !== '' && (
              <div style={{ color: '#E46470' }}>{categoryError}</div>
            )}
            {categorySuccess !== '' && (
              <div style={{ color: '#4BB543' }}>{categorySuccess}</div>
            )} */}
                    </div>
                    <div className="col-4 col-lg-4">
                        <label className="d-none d-md-flex">
                            <h6 className="invisible">Countries</h6>
                        </label>
                        <div className="form-group">
                            <button className="btn add-btn" onClick={submit}>
                                <i className="fas fa-plus-circle"></i>
                                {mode && mode == "add" ? "Add" : "Update"}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <Table data={countries} config={config} actions={actions} />
        </>
    );
}
