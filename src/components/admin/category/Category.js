import React from "react";

export default function Category() {
    return (
        <>
            <div className="category-list-cnt">
                <div></div>
                <div className="category-nav d-none">
                    <div className="category-ul mx-3 mx-md-0">
                        <ul
                            className="nav nav-pills mb-4 m-border-0"
                            id="pills-tab"
                            role="tablist"
                        >
                            <li className="nav-item">
                                <a
                                    className="nav-link active"
                                    id="pills-category-tab"
                                >
                                    Kategorijos
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className="nav-link"
                                    id="pills-sub-category-tab"
                                >
                                    Sub-Kategorijos
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className="nav-link"
                                    id="pills-inner-category-tab"
                                >
                                    Inner-Kategorijos
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className="nav-link"
                                    id="pills-inner-category-tab"
                                >
                                    Inner-Kategorijos2
                                </a>
                            </li>
                            <li className="nav-item li-btn d-none d-md-inline-block">
                                <a href="/category-add">
                                    <button className="btn add-btn">
                                        <img
                                            src="https://img.icons8.com/material-outlined/24/000000/plus--v1.png"
                                            alt="img"
                                        />
                                        Pridėti kategoriją
                                    </button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="category-input">
                    <div className="row w-100">
                        <div className="col-12 col-md-8 col-lg-3">
                            <div className="form-group">
                                <label>
                                    <h6 className="">Category</h6>
                                </label>
                                <div className="form-input">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Category"
                                        value=""
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="col-4 col-lg-3">
                            <label className="d-none d-md-flex">
                                <h6 className="invisible">Category</h6>
                            </label>
                            <div className="form-group">
                                <button className="btn add-btn">
                                    <i className="fas fa-plus-circle"></i>Add
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="category-list">
                    <table className="table table-striped mb-0 category-table">
                        <thead>
                            <tr>
                                <th className="no-th" scope="col">
                                    Nr.
                                </th>
                                <th scope="col">Kategorija</th>
                                <th scope="col">Valdymas</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </>
    );
}
