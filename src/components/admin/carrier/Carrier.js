import React, { useEffect, useState } from "react";
import { axiosInstance } from "../../../utils/axios";
import {
    get_carriers,
    add_carrier,
    get_countries,
} from "../../../utils/constants";
import Table from "../Table";

export default function Carrier() {
    const [mode, setMode] = useState("add");
    const [carrier, setCarrier] = useState("");
    const [carrierId, setCarrierId] = useState("");
    const [country, setCountry] = useState("");
    const [countryId, setCountryId] = useState("");
    const [carriers, setCarriers] = useState([]);
    const [countries, setCountries] = useState([]);

    useEffect(async () => {
        const countryResponse = await axiosInstance.get(get_countries);
        const axiosResponse = await axiosInstance.get(get_carriers);
        console.log("get carriers: ", countryResponse.data.data.docs);
        setCarriers(axiosResponse.data.data.docs);
        setCountries(countryResponse.data.data.docs);
        setCountry(countryResponse.data.data.docs[0].country);
        setCountry(countryResponse.data.data.docs[0]._id);
        console.log(
            "get countries: ",
            countries,
            carriers,
            countryResponse.data.data.docs
        );
    }, []);

    const config = [
        {
            label: "Nr.",
            key: "_id",
        },
        {
            label: "Carrier",
            key: "carrier",
        },
    ];

    const actions = [
        {
            label: "Edit",
            icon: "fa fa-edit",
            className: "btn-primary",
            onClick: async (id) => {
                setMode("edit");
                console.log(`editing ${id}`);
                const editCarrier = carriers.find(
                    (carrier) => carrier._id == id
                );
                console.log(editCarrier);
                setCarrier(editCarrier.carrier);
                setCarrierId(editCarrier._id);
                setCountry(country);
                setCountryId(editCarrier._id);
            },
        },
        {
            label: "Delete",
            icon: "fa fa-trash",
            className: "btn-danger",
            onClick: async (id) => {
                console.log(`deleting ${id}`);
                const deleteCarrier = carriers.find(
                    (carrier) => carrier._id == id
                );
                console.log(deleteCarrier);
                const con = window.confirm("are you sure?");
                if (con) {
                    const deleteResponse = await axiosInstance.delete(
                        get_carriers + deleteCarrier._id
                    );
                    console.log(deleteResponse);
                    if (
                        deleteResponse.status == 200 &&
                        deleteResponse.data.success == true
                    ) {
                        alert("Carrier deleted successfully.");
                    } else {
                        alert("Something went wrong! please try again.");
                    }
                }
            },
        },
    ];

    const onChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        if (name == "carrier") {
            setCarrier(value);
        }
        if (name == "country") {
            setCountry(value);
        }
    };

    const submit = async (e) => {
        console.log("submit: ", carrier, country);
        if (carrier == "" || country == "") {
            alert("Please fill the form up.");
            return;
        }
        if (mode == "add") {
            const response = await axiosInstance.post(add_carrier, {
                carrier: carrier,
                countryId: country,
            });
            if (response.status == 201 && response.data.success == true) {
                alert("Your carrier added successfully.");
                setCarrier("");
            } else {
                alert("Something went wrong! please try again.");
            }
        } else {
            const response = await axiosInstance.patch(
                get_carriers + carrierId,
                {
                    carrier: carrier,
                    countryId: country,
                }
            );
            if (response.status == 200 && response.data.success == true) {
                alert("Your carrier updated successfully.");
                setCarrier("");
            } else {
                alert("Something went wrong! please try again.");
            }
        }
    };

    return (
        <>
            <div className="category-input">
                <div className="row w-100">
                    <div className="col-12 col-md-6 col-lg-4">
                        <div className="form-group">
                            <label>
                                <h6 className="">Country</h6>
                            </label>
                            <div className="form-input">
                                <select
                                    value={countryId}
                                    name="country"
                                    onChange={(e) => {
                                        onChange(e);
                                    }}
                                >
                                    <option disabled>Please select...</option>
                                    {countries.map((country) => (
                                        <option
                                            value={country._id}
                                            key={country._id}
                                        >
                                            {country.country}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </div>
                        <div className="form-group">
                            <label>
                                <h6 className="">Carrier</h6>
                            </label>
                            <div className="form-input">
                                <input
                                    name="carrier"
                                    type="text"
                                    value={carrier}
                                    className="form-control"
                                    placeholder="Enter a carrier name"
                                    onChange={(e) => onChange(e)}
                                />
                            </div>
                        </div>
                        {/* {categoryError !== '' && (
              <div style={{ color: '#E46470' }}>{categoryError}</div>
            )}
            {categorySuccess !== '' && (
              <div style={{ color: '#4BB543' }}>{categorySuccess}</div>
            )} */}
                    </div>
                    <div className="col-4 col-lg-4">
                        <label className="d-none d-md-flex">
                            <h6 className="invisible">Carrier</h6>
                        </label>
                        <div className="form-group">
                            <button className="btn add-btn" onClick={submit}>
                                <i className="fas fa-plus-circle"></i>
                                {mode && mode == "add" ? "Add" : "Update"}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <Table data={carriers} config={config} actions={actions} />
        </>
    );
}
