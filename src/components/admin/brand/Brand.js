import React, { useEffect, useState } from "react";
import { axiosInstance } from "../../../utils/axios";
import { get_brands, add_brand } from "../../../utils/constants";
import Table from "../Table";

export default function Brand() {
    const [mode, setMode] = useState("add");
    const [brand, setBrand] = useState("");
    const [brandId, setBrandId] = useState("");
    const [brands, setBrands] = useState([]);

    useEffect(async () => {
        const axiosResponse = await axiosInstance.get(get_brands);
        console.log("get brands: ", axiosResponse);
        setBrands(axiosResponse.data.data.docs);
    }, []);

    const config = [
        {
            label: "Nr.",
            key: "_id",
        },
        {
            label: "Brand",
            key: "brand",
        },
    ];

    const actions = [
        {
            label: "Edit",
            icon: "fa fa-edit",
            className: "btn-primary",
            onClick: async (id) => {
                setMode("edit");
                console.log(`editing ${id}`);
                const editBrand = brands.find((brand) => brand._id == id);
                console.log(editBrand);
                setBrand(editBrand.brand);
                setBrandId(editBrand._id);
            },
        },
        {
            label: "Delete",
            icon: "fa fa-trash",
            className: "btn-danger",
            onClick: async (id) => {
                console.log(`deleting ${id}`);
                const deleteBrand = brands.find((brand) => brand._id == id);
                console.log(deleteBrand);
                const con = window.confirm("are you sure?");
                if (con) {
                    const deleteResponse = await axiosInstance.delete(
                        get_brands + deleteBrand._id
                    );
                    console.log(deleteResponse);
                    if (
                        deleteResponse.status == 200 &&
                        deleteResponse.data.success == true
                    ) {
                        alert("Brand deleted successfully.");
                    } else {
                        alert("Something went wrong! please try again.");
                    }
                }
            },
        },
    ];

    const onChange = (e) => {
        setBrand(e.target.value);
    };

    const submit = async (e) => {
        console.log("submit: ", brand);
        if (brand == "") {
            alert("Please enter a brand name.");
            return;
        }

        if (mode == "add") {
            const response = await axiosInstance.post(add_brand, {
                brand: brand,
            });
            if (response.status == 201 && response.data.success == true) {
                alert("Your brand added successfully.");
                setBrand("");
                setBrandId("");
                setMode("add");
            } else {
                alert("Something went wrong! please try again.");
            }
        }
        if (mode == "edit") {
            const response = await axiosInstance.patch(get_brands + brandId, {
                brand: brand,
            });
            if (response.status == 200 && response.data.success == true) {
                alert("Your brand updated successfully.");
                setBrand("");
                setBrandId("");
                setMode("add");
            } else {
                alert("Something went wrong! please try again.");
            }
        }
    };

    return (
        <>
            <div className="category-input">
                <div className="row w-100">
                    <div className="col-12 col-md-6 col-lg-4">
                        <div className="form-group">
                            <label>
                                <h6 className="">Brand</h6>
                            </label>
                            <div className="form-input">
                                <input
                                    type="text"
                                    value={brand}
                                    className="form-control"
                                    placeholder="Enter a brand name"
                                    onChange={(e) => onChange(e)}
                                />
                            </div>
                        </div>
                        {/* {categoryError !== '' && (
              <div style={{ color: '#E46470' }}>{categoryError}</div>
            )}
            {categorySuccess !== '' && (
              <div style={{ color: '#4BB543' }}>{categorySuccess}</div>
            )} */}
                    </div>
                    <div className="col-4 col-lg-4">
                        <label className="d-none d-md-flex">
                            <h6 className="invisible">Brands</h6>
                        </label>
                        <div className="form-group">
                            <button className="btn add-btn" onClick={submit}>
                                <i className="fas fa-plus-circle"></i>
                                {mode && mode == "add" ? "Add" : "Update"}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <Table data={brands} config={config} actions={actions} />
        </>
    );
}
