import React from "react";
import Table from "../Table";

export default function Material() {
  const data = [
    {
      id: 1,
      nr: 1,
      material: "delux",
    },
    {
      id: 2,
      nr: 2,
      material: "baldai",
    },
  ];
  const config = [
    {
      label: "Nr.",
      key: "nr",
    },
    {
      label: "Material",
      key: "material",
    },
  ];

  const actions = [
    {
      label: "Edit",
      icon: "fa fa-edit",
      className: "btn-primary",
      onClick: (id) => {
        console.log(`editing ${id}`);
      },
    },
    {
      label: "Delete",
      icon: "fa fa-trash",
      className: "btn-danger",
      onClick: (id) => {
        console.log(`deleting ${id}`);
      },
    },
  ];

  const handler = [
    {
      label: "Material",
      className: "",
      placeholder: "Enter a material name",
      onChange: (id) => {
        console.log(`${id.target.value}`);
      },
    },
  ];

  return (
    <>
      <Table data={data} config={config} actions={actions} handler={handler} />
    </>
  );
}
