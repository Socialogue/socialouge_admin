import React from "react";
import { Link } from "react-router-dom";

export default function Sidebar() {
  return (
    <div className="left-sidebar">
      <div>
        <Link className="back-link" to="/">
          <span>back to home</span>
        </Link>
      </div>
      <ul className="navbar-nav">
        <li className="nav-item nav-profile">
          <a href="#" className="nav-link">
            <div className="profile-image">
              <img className="img-xs rounded-circle" src="" alt="img" />
              <div className="dot-indicator bg-success"></div>
            </div>
            <div className="profile-txt">
              <p className="profile-name">Tamal Mindaugas</p>
              <p className="designation">Premium user</p>
            </div>
          </a>
        </li>
        <li className="nav-item cat-li active">
          <Link className="nav-link cat-link" to="/admin/categories">
            <span className="menu-title">
              Categories <i className="fas fa-arrow-right"></i>
            </span>
          </Link>
          <div className="sub-menu">
            <ul className="nav flex-column">
              <li className="nav-item">
                <a className="nav-link">Category</a>
              </li>
              <li className="nav-item">
                <a className="nav-link">Sub Category</a>
              </li>
              <li className="nav-item">
                <a className="nav-link">Inner Category</a>
              </li>
              <li className="nav-item">
                <a className="nav-link">Inner Category2</a>
              </li>
            </ul>
          </div>
        </li>

        <li className="nav-item cat-li">
          <Link className="nav-link cat-link" to="/admin/sub-categories">
            <span className="menu-title">
              Sub Categories <i className="fas fa-arrow-right"></i>
            </span>
          </Link>
        </li>
        <li className="nav-item cat-li">
          <Link className="nav-link cat-link" to="/admin/inner-categories">
            <span className="menu-title">
              Inner Categories <i className="fas fa-arrow-right"></i>
            </span>
          </Link>
        </li>
        <li className="nav-item cat-li">
          <Link className="nav-link cat-link" to="/admin/colors">
            <span className="menu-title">Color</span>
          </Link>
        </li>
        <li className="nav-item cat-li">
          <Link className="nav-link cat-link" to="/admin/men-size">
            <span className="menu-title">Men Size</span>
          </Link>
        </li>
        <li className="nav-item cat-li">
          <Link className="nav-link cat-link" to="/admin/women-size">
            <span className="menu-title">Women Size</span>
          </Link>
        </li>
        <li className="nav-item cat-li">
          <Link className="nav-link cat-link" to="/admin/brand">
            <span className="menu-title">Brand</span>
          </Link>
        </li>
        <li className="nav-item cat-li">
          <Link className="nav-link cat-link" to="/admin/season">
            <span className="menu-title">Season</span>
          </Link>
        </li>
        <li className="nav-item cat-li">
          <Link className="nav-link cat-link" to="/admin/material">
            <span className="menu-title">Material</span>
          </Link>
        </li>
        <li className="nav-item cat-li">
          <Link className="nav-link cat-link" to="/admin/countries">
            <span className="menu-title">Countries</span>
          </Link>
        </li>
        <li className="nav-item cat-li">
          <Link className="nav-link cat-link" to="/admin/carrier">
            <span className="menu-title">Carrier</span>
          </Link>
        </li>
      </ul>
    </div>
  );
}
