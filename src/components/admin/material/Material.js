import React, { useEffect, useState } from "react";
import { axiosInstance } from "../../../utils/axios";
import { get_materials, add_material } from "../../../utils/constants";
import Table from "../Table";

export default function Material() {
    const [mode, setMode] = useState("add");
    const [material, setMaterial] = useState("");
    const [materialId, setMaterialId] = useState("");
    const [materials, setMaterials] = useState([]);

    useEffect(async () => {
        const axiosResponse = await axiosInstance.get(get_materials);
        console.log("get materials: ", axiosResponse);
        setMaterials(axiosResponse.data.data.docs);
    }, []);

    const config = [
        {
            label: "Nr.",
            key: "_id",
        },
        {
            label: "Material",
            key: "material",
        },
    ];

    const actions = [
        {
            label: "Edit",
            icon: "fa fa-edit",
            className: "btn-primary",
            onClick: async (id) => {
                setMode("edit");
                console.log(`editing ${id}`);
                const editMaterial = materials.find(
                    (material) => material._id == id
                );
                console.log(editMaterial);
                setMaterial(editMaterial.material);
                setMaterialId(editMaterial._id);
            },
        },
        {
            label: "Delete",
            icon: "fa fa-trash",
            className: "btn-danger",
            onClick: async (id) => {
                console.log(`deleting ${id}`);
                const deleteMaterial = materials.find(
                    (material) => material._id == id
                );
                console.log(deleteMaterial);
                const con = window.confirm("are you sure?");
                if (con) {
                    const deleteResponse = await axiosInstance.delete(
                        get_materials + deleteMaterial._id
                    );
                    console.log(deleteResponse);
                    if (
                        deleteResponse.status == 200 &&
                        deleteResponse.data.success == true
                    ) {
                        alert("Material deleted successfully.");
                    } else {
                        alert("Something went wrong! please try again.");
                    }
                }
            },
        },
    ];

    const onChange = (e) => {
        setMaterial(e.target.value);
    };

    const submit = async (e) => {
        console.log("submit: ", material);
        if (material == "") {
            alert("Please enter a material name.");
            return;
        }
        if (mode == "add") {
            const response = await axiosInstance.post(add_material, {
                material: material,
            });
            if (response.status == 201 && response.data.success == true) {
                alert("Your material added successfully.");
                setMaterial("");
                setMode("add");
                setMaterialId("");
            } else {
                alert("Something went wrong! please try again.");
            }
        } else {
            const response = await axiosInstance.patch(
                get_materials + materialId,
                {
                    material: material,
                }
            );
            if (response.status == 200 && response.data.success == true) {
                alert("Your material updated successfully.");
                setMaterial("");
                setMode("add");
                setMaterialId("");
            } else {
                alert("Something went wrong! please try again.");
            }
        }
    };

    return (
        <>
            <div className="category-input">
                <div className="row w-100">
                    <div className="col-12 col-md-6 col-lg-4">
                        <div className="form-group">
                            <label>
                                <h6 className="">Material</h6>
                            </label>
                            <div className="form-input">
                                <input
                                    type="text"
                                    value={material}
                                    className="form-control"
                                    placeholder="Enter a material name"
                                    onChange={(e) => onChange(e)}
                                />
                            </div>
                        </div>
                        {/* {categoryError !== '' && (
              <div style={{ color: '#E46470' }}>{categoryError}</div>
            )}
            {categorySuccess !== '' && (
              <div style={{ color: '#4BB543' }}>{categorySuccess}</div>
            )} */}
                    </div>
                    <div className="col-4 col-lg-4">
                        <label className="d-none d-md-flex">
                            <h6 className="invisible">Materials</h6>
                        </label>
                        <div className="form-group">
                            <button className="btn add-btn" onClick={submit}>
                                <i className="fas fa-plus-circle"></i>
                                {mode && mode == "add" ? "Add" : "Update"}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <Table data={materials} config={config} actions={actions} />
        </>
    );
}
