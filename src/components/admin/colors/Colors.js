import React, { useEffect, useState } from "react";
import { axiosInstance } from "../../../utils/axios";
import { add_color, get_colors } from "../../../utils/constants";
import Table from "../Table";

export default function Colors() {
    const [mode, setMode] = useState("add");
    const [color, setColor] = useState("");
    const [colorId, setColorId] = useState("");
    const [colors, setColors] = useState([]);

    useEffect(async () => {
        const axiosResponse = await axiosInstance.get(get_colors);
        console.log("get colors: ", axiosResponse);
        setColors(axiosResponse.data.data.docs);
    }, []);

    const config = [
        {
            label: "Nr.",
            key: "_id",
        },
        {
            label: "Color",
            key: "color",
        },
    ];

    const actions = [
        {
            label: "Edit",
            icon: "fa fa-edit",
            className: "btn-primary",
            onClick: async (id) => {
                setMode("edit");
                console.log(`editing ${id}`);
                const editColor = colors.find((color) => color._id == id);
                console.log(editColor);
                setColor(editColor.color);
                setColorId(editColor._id);
            },
        },
        {
            label: "Delete",
            icon: "fa fa-trash",
            className: "btn-danger",
            onClick: async (id) => {
                console.log(`deleting ${id}`);
                const deleteColor = colors.find((color) => color._id == id);
                console.log(deleteColor);
                const con = window.confirm("are you sure?");
                if (con) {
                    const deleteResponse = await axiosInstance.delete(
                        get_colors + deleteColor._id
                    );
                    console.log(deleteResponse);
                    if (
                        deleteResponse.status == 200 &&
                        deleteResponse.data.success == true
                    ) {
                        alert("Color deleted successfully.");
                    } else {
                        alert("Something went wrong! please try again.");
                    }
                }
            },
        },
    ];

    const onChange = (e) => {
        setColor(e.target.value);
    };

    const submit = async (e) => {
        console.log("submit: ", color);
        if (color == "") {
            alert("Please enter a color name.");
            return;
        }

        let response;

        if (mode == "add") {
            response = await axiosInstance.post(add_color, {
                color: color,
            });
            if (response.status == 201 && response.data.success == true) {
                alert("Your color added successfully.");
                setColor("");
                setMode("add");
                setColorId("");
            } else {
                alert("Something went wrong! please try again.");
            }
        } else {
            response = await axiosInstance.patch(get_colors + colorId, {
                color: color,
            });
            if (response.status == 200 && response.data.success == true) {
                alert("Your color updated successfully.");
                setColor("");
                setMode("add");
                setColorId("");
            } else {
                alert("Something went wrong! please try again.");
            }
        }
    };

    return (
        <>
            <div className="category-input">
                <div className="row w-100">
                    <div className="col-12 col-md-6 col-lg-4">
                        <div className="form-group">
                            <label>
                                <h6 className="">Color</h6>
                            </label>
                            <div className="form-input">
                                <input
                                    type="text"
                                    value={color}
                                    className="form-control"
                                    placeholder="Enter a color name"
                                    onChange={(e) => onChange(e)}
                                />
                            </div>
                        </div>
                        {/* {categoryError !== '' && (
              <div style={{ color: '#E46470' }}>{categoryError}</div>
            )}
            {categorySuccess !== '' && (
              <div style={{ color: '#4BB543' }}>{categorySuccess}</div>
            )} */}
                    </div>
                    <div className="col-4 col-lg-4">
                        <label className="d-none d-md-flex">
                            <h6 className="invisible">Category</h6>
                        </label>
                        <div className="form-group">
                            <button className="btn add-btn" onClick={submit}>
                                <i className="fas fa-plus-circle"></i>
                                {mode && mode == "add" ? "Add" : "Update"}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <Table data={colors} config={config} actions={actions} />
        </>
    );
}
