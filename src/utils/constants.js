export const api_base_url = "http://localhost:5000/";

export const add_color = "admin/colors/create";
export const get_colors = "admin/colors/";

export const add_sizes = "admin/sizes/create";
export const get_sizes = "admin/sizes/";

export const add_brand = "admin/brands/create";
export const get_brands = "admin/brands/";

export const add_season = "admin/seasons/create";
export const get_seasons = "admin/seasons/";

export const add_material = "admin/materials/create";
export const get_materials = "admin/materials/";

export const add_country = "admin/countries/create";
export const get_countries = "admin/countries/";

export const add_carrier = "admin/carriers/create";
export const get_carriers = "admin/carriers/";

export const add_category = "admin/categories/create";
export const get_categories = "admin/categories/";

export const add_sub_category = "admin/sub-categories/create";
export const get_sub_categories = "admin/categories/sub-categories/";
export const delete_sub_categories = "admin/sub-categories/";

export const add_inner_category = "admin/inner-categories/create";
export const get_inner_categories = "admin/sub-categories/inner-category/";
export const delete_inner_categories = "admin/inner-categories/";

export const add_inner_category_two = "admin/inner-categories2/create";
export const get_inner_categories_two =
    "admin/inner-categories/inner-categories2/";
export const delete_inner_categories_two = "admin/inner-categories2/";

// category for sizes

export const MEN_CLOTHE_SIZE = 1;
export const MEN_PENTS_SIZE = 2;
export const MEN_SHOE_SIZE = 3;
export const WOMEN_CLOTHE_SIZE = 4;
export const WOMEN_PENT_SIZE = 5;
export const WOMEN_SHOE_SIZE = 6;
export const KID_BOYS_CLOTHE_SIZE = 7;
export const KID_BOYS_PENT_SIZE = 8;
export const KID_BOYS_SHOE_SIZE = 9;
export const KID_GIRLS_CLOTHE_SIZE = 10;
export const KID_GIRLS_SHOE_SIZE = 11;
export const KID_GIRLS_PENT_SIZE = 12;
export const BABIES_CLOTHE_SIZE = 13;
export const BABIES_SHOE_SIZE = 14;
