import React, { useEffect, useState } from "react";
import { axiosInstance } from "../../../utils/axios";
import { add_season, get_seasons } from "../../../utils/constants";
import Table from "../Table";

export default function Season() {
    const [mode, setMode] = useState("add");
    const [seasonId, setSeasonId] = useState("");
    const [season, setSeason] = useState("");
    const [seasons, setSeasons] = useState([]);

    useEffect(async () => {
        const axiosResponse = await axiosInstance.get(get_seasons);
        console.log("get seasons: ", axiosResponse);
        setSeasons(axiosResponse.data.data.docs);
    }, []);

    const config = [
        {
            label: "Nr.",
            key: "_id",
        },
        {
            label: "Season",
            key: "season",
        },
    ];

    const actions = [
        {
            label: "Edit",
            icon: "fa fa-edit",
            className: "btn-primary",
            onClick: async (id) => {
                setMode("edit");
                console.log(`editing ${id}`);
                const editSeason = seasons.find((season) => season._id == id);
                console.log(editSeason);
                setSeason(editSeason.season);
                setSeasonId(editSeason._id);
            },
        },
        {
            label: "Delete",
            icon: "fa fa-trash",
            className: "btn-danger",
            onClick: async (id) => {
                console.log(`deleting ${id}`);
                const deleteSeason = seasons.find((season) => season._id == id);
                console.log(deleteSeason);
                const con = window.confirm("are you sure?");
                if (con) {
                    const deleteResponse = await axiosInstance.delete(
                        get_seasons + deleteSeason._id
                    );
                    console.log(deleteResponse);
                    if (
                        deleteResponse.status == 200 &&
                        deleteResponse.data.success == true
                    ) {
                        alert("Season deleted successfully.");
                    } else {
                        alert("Something went wrong! please try again.");
                    }
                }
            },
        },
    ];

    const onChange = (e) => {
        setSeason(e.target.value);
    };

    const submit = async (e) => {
        console.log("submit: ", season);
        if (season == "") {
            alert("Please enter a season name.");
            return;
        }
        if (mode == "add") {
            const response = await axiosInstance.post(add_season, {
                season: season,
            });
            if (response.status == 201 && response.data.success == true) {
                alert("Your season added successfully.");
                setSeason("");
                setSeasonId("");
                setMode("add");
            } else {
                alert("Something went wrong! please try again.");
            }
        } else {
            const response = await axiosInstance.patch(get_seasons + seasonId, {
                season: season,
            });
            if (response.status == 200 && response.data.success == true) {
                alert("Your season updated successfully.");
                setSeason("");
                setSeasonId("");
                setMode("add");
            } else {
                alert("Something went wrong! please try again.");
            }
        }
    };

    return (
        <>
            <div className="category-input">
                <div className="row w-100">
                    <div className="col-12 col-md-6 col-lg-4">
                        <div className="form-group">
                            <label>
                                <h6 className="">Season</h6>
                            </label>
                            <div className="form-input">
                                <input
                                    type="text"
                                    value={season}
                                    className="form-control"
                                    placeholder="Enter a season name"
                                    onChange={(e) => onChange(e)}
                                />
                            </div>
                        </div>
                        {/* {categoryError !== '' && (
              <div style={{ color: '#E46470' }}>{categoryError}</div>
            )}
            {categorySuccess !== '' && (
              <div style={{ color: '#4BB543' }}>{categorySuccess}</div>
            )} */}
                    </div>
                    <div className="col-4 col-lg-4">
                        <label className="d-none d-md-flex">
                            <h6 className="invisible">Seasons</h6>
                        </label>
                        <div className="form-group">
                            <button className="btn add-btn" onClick={submit}>
                                <i className="fas fa-plus-circle"></i>
                                {mode && mode == "add" ? "Add" : "Update"}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <Table data={seasons} config={config} actions={actions} />
        </>
    );
}
