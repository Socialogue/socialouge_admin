import React, { Component } from "react";

class Size extends Component {
    handleSizeInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.props.handleSizeInput(name, value);
    };

    submit = () => {
        this.props.submit(this.props.category);
    };

    render() {
        return (
            <div>
                <div className="row mb-3">
                    <div className="form-group col-12 col-md-6 col-lg-3">
                        <label>
                            <h6>Type size(eu)</h6>
                        </label>
                        <input
                            onChange={this.handleSizeInput}
                            placeholder="please enter size"
                            name="sizeEu"
                            className="form-control"
                            type="text"
                        />
                    </div>
                    <div className="form-group  col-12 col-md-6 col-lg-3">
                        <label>
                            <h6>Type size(uk)</h6>
                        </label>
                        <input
                            onChange={this.handleSizeInput}
                            placeholder="please enter size"
                            name="sizeUk"
                            className="form-control"
                            type="text"
                        />
                    </div>
                    <div className="form-group  col-12 col-md-6 col-lg-3">
                        <label>
                            <h6>Type size(lt)</h6>
                        </label>
                        <input
                            onChange={this.handleSizeInput}
                            placeholder="please enter size"
                            name="sizeLt"
                            className="form-control"
                            type="text"
                        />
                    </div>
                    <div className="form-group  col-12 col-md-6 col-lg-3">
                        <label>
                            <h6>Type size(us)</h6>
                        </label>
                        <input
                            onChange={this.handleSizeInput}
                            placeholder="please enter size"
                            name="sizeUs"
                            className="form-control"
                            type="text"
                        />
                    </div>
                    <div className="col-12">
                        <button
                            className="btn btn-primary mr-2"
                            onClick={this.submit}
                        >
                            <i className="fas fa-plus"></i>
                            Save
                        </button>
                        <button className="btn btn-primary">Cancel</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Size;
