import React, { Component } from "react";
import {
    BABIES_CLOTHE_SIZE,
    BABIES_SHOE_SIZE,
    KID_BOYS_CLOTHE_SIZE,
    KID_BOYS_PENT_SIZE,
    KID_BOYS_SHOE_SIZE,
    KID_GIRLS_CLOTHE_SIZE,
    KID_GIRLS_PENT_SIZE,
    KID_GIRLS_SHOE_SIZE,
    MEN_CLOTHE_SIZE,
    MEN_PENTS_SIZE,
    MEN_SHOE_SIZE,
    WOMEN_CLOTHE_SIZE,
    WOMEN_PENT_SIZE,
    WOMEN_SHOE_SIZE,
} from "../../src/utils/constants";

export default class Assign extends Component {
    render() {
        const { data } = this.props;
        console.log("sizes: ", data);
            // if (!data.sizes) {
            //     return null;
            // }
        return (
            <div>
                <div
                    className={
                        this.props.showModal
                            ? "modal d-block show assign-modal"
                            : "modal"
                    }
                    tabindex="-1"
                    role="dialog"
                >
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Modal title</h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={this.props.toggleModal}
                                >
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="as-tlt">
                                    <label> Assign Color</label>
                                    <input
                                        type="checkbox"
                                        name="color"
                                        //   defaultChecked={colorCheck}
                                        onChange={(e) =>
                                            this.props.onChangeAssign(
                                                e,
                                                "color"
                                            )
                                        }
                                    />
                                </div>

                                <div className="size-as-list">
                                    <div className="size-as-tlt">Men Sizes</div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            1. Men clothes size - all clothes
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="menClothes"
                                            // checked={}
                                            // defaultChecked={data.sizes.includes(
                                            //     MEN_CLOTHE_SIZE
                                            // )}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    MEN_CLOTHE_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            2. Men pent size - all pent
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="menPents"
                                            // defaultChecked={selectMenSizes.menPents}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    MEN_PENTS_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            3. Men shoes size - all shoes
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="menShoes"
                                            // defaultChecked={selectMenSizes.menShoes}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    MEN_SHOE_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                </div>

                                <div className="size-as-list">
                                    <div className="size-as-tlt">
                                        Women Sizes
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            1. Women clothes size - all clothes
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="womenClothes"
                                            // defaultChecked={selectWomenSizes.womenClothes}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    WOMEN_CLOTHE_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            2. Women pent size - all pent
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="womenPents"
                                            // defaultChecked={selectWomenSizes.womenPents}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    WOMEN_PENT_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            3. Women shoes size - all shoes
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="womenShoes"
                                            // defaultChecked={selectWomenSizes.womenShoes}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    WOMEN_SHOE_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                </div>
                                <div className="size-as-list">
                                    <div className="size-as-tlt">
                                        Kid boys Sizes
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            1. Kid clothes size - all clothes
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="kidBoysClothes"
                                            // defaultChecked={selectKidBoysSizes.kidBoysClothes}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    KID_BOYS_CLOTHE_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            2. Kid pent size - all pent
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="kidBoysPents"
                                            // defaultChecked={selectKidBoysSizes.kidBoysPents}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    KID_BOYS_PENT_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            3. Kid shoes size - all shoes
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="kidBoysShoes"
                                            // defaultChecked={selectKidBoysSizes.kidBoysShoes}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    KID_BOYS_SHOE_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                </div>

                                <div className="size-as-list">
                                    <div className="size-as-tlt">
                                        Kid girls Sizes
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            1. Kid clothes size - all clothes
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="kidGirlsClothes"
                                            // defaultChecked={selectKidGirlsSizes.kidGirlsClothes}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    KID_GIRLS_CLOTHE_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            2. Kid pent size - all pent
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="kidGirlsPents"
                                            // defaultChecked={selectKidGirlsSizes.kidGirlsPents}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    KID_GIRLS_PENT_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            3. Kid shoes size - all shoes
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="kidGirlsShoes"
                                            // defaultChecked={selectKidGirlsSizes.kidGirlsShoes}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    KID_GIRLS_SHOE_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                </div>

                                <div className="size-as-list">
                                    <div className="size-as-tlt">
                                        Babies Sizes
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            1. Babies clothes size - all clothes
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="babiesClothes"
                                            // defaultChecked={selectBabiesSizes.babiesClothes}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    BABIES_CLOTHE_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                    <div className="size-as">
                                        <label>
                                            {" "}
                                            2. Babies pent size - all pent
                                        </label>
                                        <input
                                            type="checkbox"
                                            name="babiesPents"
                                            // defaultChecked={selectBabiesSizes.babiesPents}
                                            onChange={(e) =>
                                                this.props.onChangeAssign(
                                                    e,
                                                    BABIES_SHOE_SIZE
                                                )
                                            }
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button
                                    type="button"
                                    className="btn btn-primary"
                                    onClick={this.props.submitAssign}
                                >
                                    Save changes
                                </button>
                                <button
                                    type="button"
                                    className="btn btn-secondary"
                                    data-dismiss="modal"
                                    onClick={this.props.toggleModal}
                                >
                                    Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
