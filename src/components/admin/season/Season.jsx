import React from "react";
import Table from "../Table";

export default function Season() {
  const data = [
    {
      id: 1,
      nr: 1,
      season: "delux",
    },
    {
      id: 2,
      nr: 2,
      season: "baldai",
    },
  ];
  const config = [
    {
      label: "Nr.",
      key: "nr",
    },
    {
      label: "Season",
      key: "season",
    },
  ];

  const actions = [
    {
      label: "Edit",
      icon: "fa fa-edit",
      className: "btn-primary",
      onClick: (id) => {
        console.log(`editing ${id}`);
      },
    },
    {
      label: "Delete",
      icon: "fa fa-trash",
      className: "btn-danger",
      onClick: (id) => {
        console.log(`deleting ${id}`);
      },
    },
  ];

  const handler = [
    {
      label: "Season",
      className: "",
      placeholder: "Enter a season name",
      onChange: (id) => {
        console.log(`${id.target.value}`);
      },
    },
  ];
  return (
    <>
      <Table data={data} config={config} actions={actions} handler={handler} />
    </>
  );
}
