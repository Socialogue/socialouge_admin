/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { Link } from "react-router-dom";

export default function Header() {
  return (
    <div>
      <div className="admin-header-section">
        <nav className="navbar">
          <div className="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
            <a style={{ display: "flex", alignItems: "center" }}>
              <img
                className="d-none d-lg-block"
                // src='https://www.bootstrapdash.com/demo/star-admin-pro/src/assets/images/logo.svg'
                src=""
                alt="Img"
                style={{ borderRadius: "100%", height: "52px", width: "52px" }}
              />

              {/* <img
                className='d-block d-lg-none'
                src='https://www.bootstrapdash.com/demo/star-admin-pro/src/assets/images/logo-mini.svg'
                alt='Img'
              /> */}
              <h3 style={{ color: "#fff", marginLeft: "20px" }}>FE Admin</h3>
            </a>
          </div>
          <div className="navbar-menu-wrapper d-flex align-items-center">
            <button className="navbar-toggler d-inline-block d-lg-none">
              <img src="https://img.icons8.com/material-rounded/24/000000/menu.png" />
            </button>
            <ul className="navbar-nav contact-navbar">
              <li className="nav-item help-txt mr-3">Help : +37064579739</li>
              <li className="nav-item dropdown language-dropdown">
                <a className="nav-link px-2 d-flex align-items-center">
                  <div className="flag-icon-holder ">
                    <img
                      className="rounded-circle"
                      src="https://cdn.britannica.com/79/4479-050-6EF87027/flag-Stars-and-Stripes-May-1-1795.jpg"
                      alt="Profile"
                    />
                  </div>
                  <span className="lan-txt d-none d-md-inline-block">
                    English
                  </span>
                </a>
                <div className="dropdown-menu py-2">
                  <a className="dropdown-item">
                    <div className="flag-icon-holder mr-2">
                      <img
                        src="https://cdn.britannica.com/79/4479-050-6EF87027/flag-Stars-and-Stripes-May-1-1795.jpg"
                        alt="Img"
                      />
                    </div>
                    <div className="dropdown-item-txt">English</div>
                  </a>
                  <a className="dropdown-item">
                    <div className="flag-icon-holder mr-2">
                      <img
                        src="https://cdn.britannica.com/79/4479-050-6EF87027/flag-Stars-and-Stripes-May-1-1795.jpg"
                        alt="Img"
                      />
                    </div>
                    <div className="dropdown-item-txt">English</div>
                  </a>
                </div>
              </li>
            </ul>
            <form className="search-form d-none d-md-block">
              <div className="form-group mb-0 ">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Search Here"
                />
              </div>
            </form>
            <ul className="navbar-nav user-navbar ml-auto">
              <li className="nav-item dropdown notification-dropdown">
                <a className="nav-link count-indicator">
                  <img src="https://img.icons8.com/material-outlined/24/000000/appointment-reminders--v1.png" />
                  <span className="count">7</span>
                </a>
                <div className="dropdown-menu">
                  <a className="dropdown-item dropdown-item-tlt">
                    <div className="dropdown-item-txt">
                      fjsdjfsljgl
                      <span className="badge badge-pill badge-primary float-right">
                        View all
                      </span>
                    </div>
                  </a>
                  <a className="dropdown-item">
                    <div className="flag-icon-holder mr-2">
                      <img
                        src="https://cdn.britannica.com/79/4479-050-6EF87027/flag-Stars-and-Stripes-May-1-1795.jpg"
                        alt="Img"
                      />
                    </div>
                    <div className="dropdown-item-txt">
                      <div className="dropdown-item-txt1">kjhkh</div>
                      <div className="dropdown-item-txt2">jljlkjlsgdghdh</div>
                    </div>
                  </a>
                  <a className="dropdown-item">
                    <div className="flag-icon-holder mr-2">
                      <img
                        src="https://cdn.britannica.com/79/4479-050-6EF87027/flag-Stars-and-Stripes-May-1-1795.jpg"
                        alt="Img"
                      />
                    </div>
                    <div className="dropdown-item-txt">
                      <div className="dropdown-item-txt1">kjhkh</div>
                      <div className="dropdown-item-txt2">jljlkjlsgdghdh</div>
                    </div>
                  </a>
                </div>
              </li>
              <li className="nav-item dropdown mesage-dropdown">
                <a className="nav-link count-indicator">
                  <img src="https://img.icons8.com/ios-glyphs/30/000000/gmail-login.png" />
                  <span className="count bg-success">3</span>
                </a>
                <div className="dropdown-menu py-2">
                  <a className="dropdown-item">
                    <div className="flag-icon-holder mr-2">
                      <img
                        src="https://cdn.britannica.com/79/4479-050-6EF87027/flag-Stars-and-Stripes-May-1-1795.jpg"
                        alt="Img"
                      />
                    </div>
                    <div className="dropdown-item-txt">English</div>
                  </a>
                  <a className="dropdown-item">
                    <div className="flag-icon-holder mr-2">
                      <img
                        src="https://cdn.britannica.com/79/4479-050-6EF87027/flag-Stars-and-Stripes-May-1-1795.jpg"
                        alt="Img"
                      />
                    </div>
                    <div className="dropdown-item-txt">English</div>
                  </a>
                </div>
              </li>
              <li className="nav-item dropdown user-dropdown ml-3 ml-md-4">
                <a className="nav-link dropdown-toggle ">
                  <img
                    className="rounded-circle user-img"
                    // src='https://www.bootstrapdash.com/demo/star-admin-pro/src/assets/images/faces/face8.jpg'
                    src=""
                    alt="Img"
                    style={{
                      borderRadius: "100%",
                      height: "50px",
                      width: "50px",
                    }}
                  />{" "}
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  );
}
