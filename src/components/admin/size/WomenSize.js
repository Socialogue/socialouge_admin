import React, { Component } from "react";
import {
    WOMEN_CLOTHE_SIZE,
    add_sizes,
    WOMEN_PENT_SIZE,
    WOMEN_SHOE_SIZE,
    get_sizes,
} from "../../../utils/constants";
import Table from "../Table";
import Size from "../Size";
import { axiosInstance } from "../../../utils/axios";

export default class WomenSize extends Component {
    state = {
        sizeId: "",
        sizeEu: "",
        sizeUs: "",
        sizeUk: "",
        sizeLt: "",
        mode: "add",
        showModal: false,
        editSize: {},
        womenClotheSizes: [],
        womenPentSizes: [],
        womenShoeSizes: [],
        sizes: [],
        category: "",
    };

    componentDidMount = async () => {
        const sizes = await axiosInstance.get(get_sizes);

        const womenClothe = sizes.data.filter(
            (size) => size.category == WOMEN_CLOTHE_SIZE
        );
        const womenPent = sizes.data.filter(
            (size) => size.category == WOMEN_PENT_SIZE
        );
        const womenShoe = sizes.data.filter(
            (size) => size.category == WOMEN_SHOE_SIZE
        );
        console.log("sizes: ", womenClothe);
        this.setState({
            sizes: sizes.data,
            womenClotheSizes: womenClothe,
            womenPentSizes: womenPent,
            womenShoeSizes: womenShoe,
        });
    };

    config = [
        {
            label: "EU",
            key: "eu",
        },
        {
            label: "UK",
            key: "uk",
        },
        {
            label: "LT",
            key: "lt",
        },
        {
            label: "US",
            key: "us",
        },
    ];

    actions = [
        {
            label: "Edit",
            icon: "fa fa-edit",
            className: "btn-primary",
            onClick: (id) => {
                const editSize = this.state.sizes.find(
                    (size) => size._id == id
                );
                this.showModal();
                console.log("editing", editSize, id);
                this.setState({
                    mode: "edit",
                    sizeEu: editSize.eu,
                    sizeUk: editSize.uk,
                    sizeLt: editSize.lt,
                    sizeUs: editSize.us,
                    sizeId: id,
                    category: editSize.category,
                });
            },
        },
        {
            label: "Delete",
            icon: "fa fa-trash",
            className: "btn-danger",
            onClick: async (id) => {
                console.log(`deleting ${id}`);
                const deleteSize = this.state.sizes.find(
                    (size) => size._id == id
                );
                const con = window.confirm("are you sure?");
                if (con) {
                    const deleteResponse = await axiosInstance.delete(
                        get_sizes + deleteSize._id
                    );
                    console.log(deleteResponse);
                    if (deleteResponse.status == 200) {
                        alert("Size deleted successfully.");
                    } else {
                        alert("Something went wrong! please try again.");
                    }
                }
            },
        },
    ];

    handleSizeInput = (name, value) => {
        console.log("size input: ", name, value);
        this.setState({
            [name]: value,
        });
    };

    handleSizeInputUpdate = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value,
        });
    };

    submit = async (category) => {
        console.log(
            "submit: ",
            this.state.sizeUk,
            this.state.sizeEu,
            this.state.sizeUs,
            this.state.sizeLt
        );
        if (
            (this.state.sizeUk == "",
            this.state.sizeEu == "",
            this.state.sizeUs == "",
            this.state.sizeLt == "")
        ) {
            alert("Please enter a size.");
            return;
        }
        if (this.state.mode == "add") {
            const response = await axiosInstance.post(add_sizes, {
                uk: this.state.sizeUk,
                eu: this.state.sizeEu,
                us: this.state.sizeUs,
                lt: this.state.sizeLt,
                category: category,
            });
            console.log("res: ", response);
            if (response.status == 201) {
                alert("Your sizes added successfully.");
                this.setState({
                    mode: "add",
                });
            } else {
                alert("Something went wrong! please try again.");
            }
        }
    };

    update = async () => {
        const id = this.state.sizeId.toString();
        console.log(
            "update: ",
            {
                uk: this.state.sizeUk,
                eu: this.state.sizeEu,
                us: this.state.sizeUs,
                lt: this.state.sizeLt,
                category: this.state.category,
            },
            id,
            get_sizes
        );
        const response = await axiosInstance.patch(get_sizes + id, {
            uk: this.state.sizeUk,
            eu: this.state.sizeEu,
            us: this.state.sizeUs,
            lt: this.state.sizeLt,
            category: this.state.category,
        });
        this.setState({
            showModal: false,
            mode: "add",
            sizeEu: "",
            sizeLt: "",
            sizeUk: "",
            sizeUs: "",
        });
        if (response.status == 200) {
            alert("Your size updated successfully.");
            this.setState({
                mode: "add",
            });
        } else {
            alert("Something went wrong! please try again.");
        }
    };

    showModal = () => {
        this.setState({
            showModal: !this.state.showModal,
        });
    };

    render() {
        return (
            <>
                <Size
                    category={WOMEN_CLOTHE_SIZE}
                    handleSizeInput={this.handleSizeInput}
                    submit={this.submit}
                    mode={this.state.mode}
                />
                <h2>Women clothes size</h2>
                <Table
                    data={this.state.womenClotheSizes}
                    config={this.config}
                    actions={this.actions}
                />
                <h2>Women pents size</h2>
                <Size
                    category={WOMEN_PENT_SIZE}
                    handleSizeInput={this.handleSizeInput}
                    submit={this.submit}
                    mode={this.state.mode}
                />
                <Table
                    data={this.state.womenPentSizes}
                    config={this.config}
                    actions={this.actions}
                />
                <h2>Women shoes size</h2>
                <Size
                    category={WOMEN_SHOE_SIZE}
                    handleSizeInput={this.handleSizeInput}
                    submit={this.submit}
                    mode={this.state.mode}
                />
                <Table
                    data={this.state.womenShoeSizes}
                    config={this.config}
                    actions={this.actions}
                />

                <div
                    className={
                        this.state.showModal ? "modal d-block show" : "modal"
                    }
                    tabindex="-1"
                    role="dialog"
                >
                    <div
                        className="modal-dialog modal-dialog-centered"
                        role="document"
                    >
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Update</h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={this.showModal}
                                >
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row mb-3">
                                    <div className="form-group col-12 col-md-6">
                                        <label>
                                            <h6>Type size(eu)</h6>
                                        </label>
                                        <input
                                            onChange={
                                                this.handleSizeInputUpdate
                                            }
                                            placeholder="please enter size"
                                            name="sizeEu"
                                            className="form-control"
                                            type="text"
                                            value={this.state.sizeEu}
                                        />
                                    </div>
                                    <div className="form-group  col-12 col-md-6 ">
                                        <label>
                                            <h6>Type size(uk)</h6>
                                        </label>
                                        <input
                                            onChange={
                                                this.handleSizeInputUpdate
                                            }
                                            placeholder="please enter size"
                                            name="sizeUk"
                                            className="form-control"
                                            type="text"
                                            value={this.state.sizeUk}
                                        />
                                    </div>
                                    <div className="form-group  col-12 col-md-6 ">
                                        <label>
                                            <h6>Type size(lt)</h6>
                                        </label>
                                        <input
                                            onChange={
                                                this.handleSizeInputUpdate
                                            }
                                            placeholder="please enter size"
                                            name="sizeLt"
                                            className="form-control"
                                            type="text"
                                            value={this.state.sizeLt}
                                        />
                                    </div>
                                    <div className="form-group  col-12 col-md-6 ">
                                        <label>
                                            <h6>Type size(us)</h6>
                                        </label>
                                        <input
                                            onChange={
                                                this.handleSizeInputUpdate
                                            }
                                            placeholder="please enter size"
                                            name="sizeUs"
                                            className="form-control"
                                            type="text"
                                            value={this.state.sizeUs}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button
                                    type="button"
                                    className="btn btn-primary"
                                    onClick={this.update}
                                >
                                    Update
                                </button>
                                <button
                                    type="button"
                                    className="btn btn-secondary"
                                    data-dismiss="modal"
                                    onClick={this.showModal}
                                >
                                    Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
