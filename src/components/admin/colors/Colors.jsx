import React from "react";
import Table from "../Table";

export default function Colors() {
  const data = [
    {
      id: 1,
      nr: 1,
      color: "red",
    },
    {
      id: 2,
      nr: 2,
      color: "baldai",
    },
  ];
  const config = [
    {
      label: "Nr.",
      key: "nr",
    },
    {
      label: "Color",
      key: "color",
    },
  ];

  const actions = [
    {
      label: "Edit",
      icon: "fa fa-edit",
      className: "btn-primary",
      onClick: (id) => {
        console.log(`editing ${id}`);
      },
    },
    {
      label: "Delete",
      icon: "fa fa-trash",
      className: "btn-danger",
      onClick: (id) => {
        console.log(`deleting ${id}`);
      },
    },
  ];

  const handler = [
    {
      label: "Color",
      className: "",
      placeholder: "Enter a color name",
      onChange: (id) => {
        console.log(`${id.target.value}`);
      },
    },
  ];

  return (
    <>
      <Table data={data} config={config} actions={actions} handler={handler} />
    </>
  );
}
