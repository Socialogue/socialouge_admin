import { lazy } from "react";
const Home = lazy(() => import("../pages/Home"));
const About = lazy(() => import("../pages/About"));
const Categories = lazy(() =>
    import("../components/admin/categories/Categories")
);
const SubCategories = lazy(() =>
    import("../components/admin/subcategories/Subcategories")
);
const InnerCategories = lazy(() =>
    import("../components/admin/innercategories/Innercategories")
);
const InnerCategoriesTwo = lazy(() =>
    import("../components/admin/innercategoriestwo/InnerCategoriestwo")
);
const Colors = lazy(() => import("../components/admin/colors/Colors"));
const MenSize = lazy(() => import("../components/admin/size/MenSize"));
const WomenSize = lazy(() => import("../components/admin/size/WomenSize"));
const KidBoySize = lazy(() => import("../components/admin/size/KidBoy"));
const kidGirlSize = lazy(() => import("../components/admin/size/KidGirl"));
const babySize = lazy(() => import("../components/admin/size/Baby"));
const Brand = lazy(() => import("../components/admin/brand/Brand"));
const Season = lazy(() => import("../components/admin/season/Season"));
const Material = lazy(() => import("../components/admin/material/Material"));
const Coutries = lazy(() => import("../components/admin/country/Countries"));
const Carrier = lazy(() => import("../components/admin/carrier/Carrier"));

export const routes = [
    {
        path: "/",
        component: Home,
        exact: true,
    },
    {
        path: "/about",
        component: About,
        exact: true,
    },
    {
        path: "/admin/categories",
        component: Categories,
        exact: true,
    },
    {
        path: "/admin/categories/:catId/sub-categories",
        component: SubCategories,
        exact: true,
    },
    {
        path:
            "/admin/categories/:catId/sub-categories/:subCatId/inner-categories",
        component: InnerCategories,
        exact: true,
    },
    {
        path:
            "/admin/categories/:catId/sub-categories/:subCatId/inner-categories/:innerCatId/inner-categories-two",
        component: InnerCategoriesTwo,
        exact: true,
    },
    {
        path: "/admin/colors",
        component: Colors,
        exact: true,
    },
    {
        path: "/admin/men-size",
        component: MenSize,
        exact: true,
    },
    {
        path: "/admin/women-size",
        component: WomenSize,
        exact: true,
    },
    {
        path: "/admin/kid-boy-size",
        component: KidBoySize,
        exact: true,
    },
    {
        path: "/admin/kid-girl-size",
        component: kidGirlSize,
        exact: true,
    },
    {
        path: "/admin/babies-size",
        component: babySize,
        exact: true,
    },
    {
        path: "/admin/brand",
        component: Brand,
        exact: true,
    },
    {
        path: "/admin/season",
        component: Season,
        exact: true,
    },
    {
        path: "/admin/material",
        component: Material,
        exact: true,
    },
    {
        path: "/admin/countries",
        component: Coutries,
        exact: true,
    },
    {
        path: "/admin/carrier",
        component: Carrier,
        exact: true,
    },
    // {
    //   path: "/admin/user",
    //   component: User,
    //   exact: true,
    // },
];
